﻿// Mancala, by Serinity

public class Mancala
{
    public int[] _board { get; set; }
    public Dictionary<int, int> _holeOpposites { get; set; }

    public Mancala()
    {
        _board = new int[14] { 4, 4, 4, 4, 4, 4, 0, 4, 4, 4, 4, 4, 4, 0 }; // Standard mancala board setup
        _holeOpposites = new Dictionary<int, int> { { 0, 12 }, { 12, 0 }, { 1, 11 }, { 11, 1 }, { 2, 10 }, { 10, 2 }, { 3, 9 }, { 9, 3 }, { 4, 8 }, { 8, 4 }, { 5, 7 }, { 7, 5 } };
    }
    
    public void DisplayBoard()
    {
        Console.ForegroundColor = ConsoleColor.Red;
        Console.WriteLine($"\n     | {_board[12]} | {_board[11]} | {_board[10]} | {_board[9]} | {_board[8]} | {_board[7]} |  ");
        Console.ForegroundColor = ConsoleColor.Blue;
        Console.WriteLine($"| {_board[13]} |-------------------------| {_board[6]} |");
        Console.ForegroundColor = ConsoleColor.Green;
        Console.WriteLine($"     | {_board[0]} | {_board[1]} | {_board[2]} | {_board[3]} | {_board[4]} | {_board[5]} |  ");
        Console.ForegroundColor = ConsoleColor.Cyan;
        Console.WriteLine($"       A   B   C   D   E   F    \n");
        Console.ForegroundColor = ConsoleColor.White;
    }

    public bool HasWon()
    {
        int piecesLeft1 = 0;
        int piecesLeft2 = 0;

        // Player 1 check
        for (int i = 0; i <= 5; i++)
        {
            piecesLeft1 += _board[i];
        }

        //Player 2 check
        for (int i = 7; i <= 12; i++)
        {
            piecesLeft2 += _board[i];
        }

        if (piecesLeft1 == 0 || piecesLeft2 == 0) return true; // If either player has no pieces left in their holes
        else return false;
    }
}
