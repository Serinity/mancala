# Mancala

## Description
This repo is a version of Mancala written entirely in C#. The player plays against a computer, which admittedly does not have much strategy to it's actions at the moment. The game board consists of 12 holes (6 per player) and 2 stores (1 per player). The store to the right is the player's store, and the holes closest to the player (bottom) are the player's holes.

## Game Rules
Each player (or computer) picks one of their holes (labeled A, B, C, D, E, F) to move their pieces out of. Pieces are placed one at a time in counter-clockwise order in each hole and store, except for the opponent's store. If a player's placement of pieces ends in their store, they get an extra turn. If a player's placement of pieces ends in an empty hole on their side of the board, they capture that piece and the opponent's pieces on the opposite hole into their store. When one player's side is empty, the opponent's pieces in their holes are automatically added to their store, and store's quantities of pieces are compared. Whoever ends the game with the most pieces in their store wins.

## Visuals
![Gameplay with stolen mechanicsm](https://media.discordapp.net/attachments/580138809191301161/1224386257409413252/image.png?ex=661d4d8f&is=660ad88f&hm=1f7774a7aef36e7b0cb65e7f9b3fb4a72aa141ddf7b3337ac88711e67d742fc4&=&format=webp&quality=lossless)

![Gameplay with landing in store repeat turn](https://media.discordapp.net/attachments/580138809191301161/1224389929161986088/image.png?ex=661d50fb&is=660adbfb&hm=1e753a3996c2e8b8b931baacc0e62852d08bbc39c9629a5eac1c2a9abf092f10&=&format=webp&quality=lossless)

![Win screen](https://media.discordapp.net/attachments/580138809191301161/1224387323886571520/image.png?ex=661d4e8e&is=660ad98e&hm=5b9b236d0205989372c984ad2ed7664d5b36b2183645438b7cdef3a42814109f&=&format=webp&quality=lossless)

## Installation
At this time, the release binary is portable! Just download and run. Otherwise, you can compile it yourself.

## Usage
Download the release package binary, or compile yourself. Enter a letter A-F to pick your hole. Everything else is automatic :)

## Support
Feel free to submit an issue or feature request. I can't promise I'll get to it, but I sure will try.

## Roadmap
Features to be added in the future:

~~1. Player 2 support~~

2. GUI

3. Smarter computer logic

~~4. Error / invalid input handling~~

## Contributing
I am not open to contributions at this time as this is a learning experience for me. Feel free to fork my project if you have a burning passion for mancala.

## License
GPLv3

## Project status
Currently working on it!
