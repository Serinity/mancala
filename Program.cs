﻿
// Mancala, by Serinity

using System.Reflection.Metadata.Ecma335;

Console.Title = "Mancala";

bool twoPlayer;

while (true)
{
    Console.WriteLine("Would you like to play against a computer, or a friend?");
    string input = Console.ReadLine().ToLower();

    twoPlayer = input switch // Set whether game is two player or computer player
    {
        "computer" => false,
        "friend" => true,
        _ => false
    };

    if (input != "computer" && input != "friend")
    {
        Console.WriteLine("Invalid selection. Please enter 'computer' or 'friend'.");
        continue;
    }
    else break;
}


Mancala mancala = new Mancala();
Player player1 = new Player(1);
Player player2 = new Player(2);
Computer computer = new Computer(2);

Console.Clear();
mancala.DisplayBoard();

while (true) //Gameplay loop
{

    RunPlayerTurn(player1);
    
    if (mancala.HasWon()) break;

    if (twoPlayer)
    {
        RunPlayerTurn(player2);
    }
    else
    {
        RunPlayerTurn(computer);
    }

    if (mancala.HasWon()) break;
}

Console.Clear();

for (int i = 0, j = 7; j < mancala._board.Length - 1; i++, j++) // Move all pieces to stores
{
    //Player 1
    mancala._board[6] += mancala._board[i];
    mancala._board[i] = 0;

    //Player 2
    mancala._board[13] += mancala._board[j];
    mancala._board[j] = 0;
}

mancala.DisplayBoard();

if (mancala._board[6] > mancala._board[13]) Console.WriteLine("Player 1 has won!");
else Console.WriteLine("Player 2 has won!");

Console.WriteLine("Press any key to exit.");
Console.ReadKey(true);

void RunPlayerTurn(Player player)
{
    char selection;
    int numSelection;
    int originalAmount;
    int landingPosition = 0;
    int opponentStore;

    if (player._playerNum == 1) opponentStore = 13;
    else opponentStore = 6;


    while (true)
    {
        // Begin Player's turn
        if (Convert.ToString(player) != "Computer") selection = player.PickHole(mancala._board);
        else selection = computer.RandomHole(mancala._board);

        numSelection = player._holePairs[selection];

        originalAmount = mancala._board[numSelection];

        for (int i = numSelection, j = numSelection; j <= numSelection + originalAmount; i++, j++)
        {
            if (i == opponentStore) i++; // If player lands in opponent's store, skip
            if (i > mancala._board.Length - 1) i = 0; // If player exceeds bounds of board length, wrap back around
            if (j == numSelection) continue; // Skip adding a piece to original hole on first time

            mancala._board[i]++;
            landingPosition = i;
        }
        mancala._board[numSelection] -= originalAmount; // Subtract original amount of pieces from origin hole

        player.Write($"{player} {player._playerNum} moved hole {selection}\n", $"{player} {player._playerNum}");

        if (player._playerNum == 1)
        {
            if (mancala._board[landingPosition] == 1 && landingPosition < 6) // If player 1 lands in an empty hole on their side
            {
                if (mancala._board[mancala._holeOpposites[landingPosition]] != 0) // If opponent has pieces to steal
                {
                    Console.WriteLine("Opponent's pieces stolen!");
                    Console.Beep();
                }
                mancala._board[6] += 1 + mancala._board[mancala._holeOpposites[landingPosition]]; // Add adjacent hole and current hole to store

                mancala._board[landingPosition] = 0; // Set landing positon to 0
                mancala._board[mancala._holeOpposites[landingPosition]] = 0; // Set adjacent hole to 0

            }
            if (landingPosition == 6)
            {
                player.Write($"{player} {player._playerNum} landed in their store. They earn another turn!\n", $"{player} {player._playerNum}");
                Console.Beep();
                mancala.DisplayBoard();
                continue;
            }
            else
            {
                mancala.DisplayBoard();
                break;
            }
        }
        else
        {
            if (mancala._board[landingPosition] == 1 && landingPosition > 6 && landingPosition != 13) // If player 2 / computer lands in an empty hole on their side
            {
                if (mancala._board[mancala._holeOpposites[landingPosition]] != 0) // If opponent has pieces to steal
                {
                    Console.WriteLine("Opponent's pieces stolen!");
                    Console.Beep();
                }
                mancala._board[13] += 1 + mancala._board[mancala._holeOpposites[landingPosition]]; // Add adjacent hole and current hole to store

                mancala._board[landingPosition] = 0; // Set landing positon to 0
                mancala._board[mancala._holeOpposites[landingPosition]] = 0; // Set adjacent hole to 0
            }

            if (landingPosition == 13)
            {
                player.Write($"{player} {player._playerNum} landed in their store. They earn another turn!\n", $"{player} {player._playerNum}");
                Console.Beep();
                mancala.DisplayBoard();
                continue;
            }
            else
            {
                mancala.DisplayBoard();
                break;
            }
        }



    }
}

