﻿// Mancala, by Serinity

public class Player
{
    public int _playerNum { get; set; }
    internal char[] _chars = new char[6] { 'A', 'B', 'C', 'D', 'E', 'F' };
    public Dictionary<char, int> _holePairs { get; set; }

    public Player(int playerNum)
    {
        _playerNum = playerNum;

        if (_playerNum == 1) _holePairs = new Dictionary<char, int> { { 'A', 0 }, { 'B', 1 }, { 'C', 2 }, { 'D', 3 }, { 'E', 4 }, { 'F', 5 } };
        else _holePairs = new Dictionary<char, int> { { 'A', 12 }, { 'B', 11 }, { 'C', 10 }, { 'D', 9 }, { 'E', 8 }, { 'F', 7 } };
    }

    public char PickHole(int[] board)
    {
        char selection = ' ';

        do
        {
            try
            {
                Write($"Player {this._playerNum}, please pick a hole that is not empty: ", $"Player {this._playerNum}");
                selection = Convert.ToChar((Console.ReadLine().ToUpper()));
                if (selection != _chars[0] && selection != _chars[1] && selection != _chars[2] && selection != _chars[3] && selection != _chars[4] && selection != _chars[5]) throw new NotSupportedException();
            }
            catch (NotSupportedException e)
            {
                Console.WriteLine("That is not a valid letter.");
                selection = ' ';
                continue;
            }
            

        } while (selection == ' ' || board[_holePairs[selection]] == 0); // Keep asking for holes until finding a spot that does not equal 0.

        return selection;

    }

    public void Write(string text, string coloredWord)
    {
        string[] normalText = text.Split(new string[] { coloredWord }, StringSplitOptions.None);

        for (int i = 0; i < normalText.Length; i++)
        {
            Console.ResetColor();
            Console.Write(normalText[i]);
            if (i != normalText.Length - 1 && coloredWord == "Player 1")
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.Write(coloredWord);
            }
            else if (i != normalText.Length - 1)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write(coloredWord);
            }
        }
    }

}
