﻿// Mancala, by Serinity

public class Computer : Player
{
    

    public Computer(int playerNum) : base(playerNum)
    {
        _playerNum = playerNum;
        _holePairs = new Dictionary<char, int> { { 'A', 12 }, { 'B', 11 }, { 'C', 10 }, { 'D', 9 }, { 'E', 8 }, { 'F', 7 } };
    }

    public char RandomHole(int[] board)
    {
        Random random = new Random();
        int randomNum = 0; // Initialize randomNum to 0 so loop runs

        do
        {
          randomNum = random.Next(0, 6);
        } while (board[_holePairs[base._chars[randomNum]]] == 0); // Keep finding a random number until finding a spot that does not equal 0

        return base._chars[randomNum];
    }

}